import { Pipe, PipeTransform } from '@angular/core';
import { OptionModel } from '../models/option.model';

@Pipe({ name: 'optionsSort' })
export class OptionsSortPipe implements PipeTransform {

	transform(value: Array<OptionModel>): Array<any> {
		return value
			.sort((a, b) => {
				if (b.rate === a.rate) {
					return parseInt(a.id) - parseInt(b.id);
				}

				return b.rate - a.rate;
			});
	}

}
