export interface OptionsHelperConfig {

	hasOption(value: string): boolean;

}
