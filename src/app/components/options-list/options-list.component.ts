import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { OptionModel } from '../../models/option.model';

@Component({
	selector: 'ua-options-list',
	templateUrl: './options-list.component.html',
	styleUrls: ['./options-list.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionsListComponent implements OnInit {

	@Input() public options: Array<OptionModel>;
	@Input() public isCompleted: boolean;

	@Output() readonly optionRemove = new EventEmitter<OptionModel>();

	constructor() {
	}

	ngOnInit(): void {
	}


	public onRemoveClick(option: OptionModel): void {
		this.optionRemove.emit(option);
	}


	public trackById = (index: number, value: { id: string }) => value.id;

}
