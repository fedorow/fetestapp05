import { Injectable, NgModule } from '@angular/core';
import { ComparisonService } from '../services/comparison.service';
import { OptionsHelperConfig } from './options-helper.config';
import { OPTIONS_HELPER } from './options-helper.token';

@Injectable()
class OptionsHelperService implements OptionsHelperConfig {

	constructor(
		public comparisonService: ComparisonService
	) {
	}

	public hasOption(value: string): boolean {
		return this.comparisonService.hasOption(value);
	}

}

@NgModule({
	providers: [
		{
			provide: OPTIONS_HELPER,
			useClass: OptionsHelperService,
			deps: [ComparisonService]
		}
	]
})
export class OptionsHelperModule {
}
