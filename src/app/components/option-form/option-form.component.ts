import { ChangeDetectionStrategy, Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { OptionsHelperConfig } from '../../tokens/options-helper.config';
import { OPTIONS_HELPER } from '../../tokens/options-helper.token';

@Component({
	selector: 'ua-option-form',
	templateUrl: './option-form.component.html',
	styleUrls: ['./option-form.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionFormComponent implements OnInit {

	public formGroup: FormGroup;
	public inputControl: FormControl;

	@Output() readonly optionAdd = new EventEmitter<string>();

	constructor(
		@Inject(OPTIONS_HELPER) private readonly optionsHelper: OptionsHelperConfig
	) {
	}

	ngOnInit(): void {
		this.initForm();
	}


	public onAddOptionClick(): void {
		if (this.inputControl.valid) {
			this.optionAdd.emit(this.inputControl.value);

			this.formGroup.reset();
		} else {
			this.formGroup.markAsDirty();
		}
	}


	private initForm(): void {
		this.inputControl = new FormControl(null, [
			Validators.required,
			this.uniqueValidator
		]);

		this.formGroup = new FormGroup({
			input: this.inputControl
		});
	}


	private uniqueValidator = (control: AbstractControl): ValidationErrors | null => {
		return this.optionsHelper.hasOption(control.value)
			? { unique: true }
			: null;
	};

}
