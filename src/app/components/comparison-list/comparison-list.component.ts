import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ComparisonPairModel } from '../../models/comparison-pair.model';

@Component({
	selector: 'ua-comparison-list',
	templateUrl: './comparison-list.component.html',
	styleUrls: ['./comparison-list.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComparisonListComponent implements OnInit {

	@Input() public pairs: Array<ComparisonPairModel>;

	@Output() readonly optionSelect = new EventEmitter<ComparisonPairModel>();

	constructor() {
	}

	ngOnInit(): void {
	}


	public onOptionClick(pair: ComparisonPairModel, selected: number): void {
		if (pair.selected === -1 || pair.selected === selected) {
			this.optionSelect.emit({
				...pair,
				selected: pair.selected === selected ? -1 : selected
			});
		}
	}


	public trackById = (index: number, value: { id: string }) => value.id;

	public isOptionEnabled = (value: ComparisonPairModel, index: number) => value.selected !== (1 - index);

	public isOptionSelected = (value: ComparisonPairModel, index: number) => value.selected === index;

}
