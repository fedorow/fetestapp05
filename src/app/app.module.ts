import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { ComparisonListComponent } from './components/comparison-list/comparison-list.component';
import { OptionFormComponent } from './components/option-form/option-form.component';
import { OptionsListComponent } from './components/options-list/options-list.component';
import { OptionsSortPipe } from './pipes/options-sort.pipe';
import { OptionsHelperModule } from './tokens/options-helper.module';

@NgModule({
	declarations: [
		AppComponent,

		OptionFormComponent,
		OptionsListComponent,
		ComparisonListComponent,

		OptionsSortPipe
	],
	imports: [
		BrowserModule,
		ReactiveFormsModule,

		OptionsHelperModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
