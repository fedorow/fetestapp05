import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ComparisonPairModel } from './models/comparison-pair.model';
import { OptionModel } from './models/option.model';
import { ComparisonService } from './services/comparison.service';

@Component({
	selector: 'ua-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

	constructor(
		public comparisonService: ComparisonService
	) {
	}

	ngOnInit(): void {
	}


	public onOptionAdd(option: string): void {
		this.comparisonService.addOption(option);
	}

	public onOptionRemove(option: OptionModel): void {
		this.comparisonService.removeOption(option.id);
	}

	public onOptionSelect(pair: ComparisonPairModel): void {
		this.comparisonService.updatePair(pair);
	}

}
