import { InjectionToken } from '@angular/core';
import { OptionsHelperConfig } from './options-helper.config';

export const OPTIONS_HELPER = new InjectionToken<OptionsHelperConfig>('optionsHelper');
