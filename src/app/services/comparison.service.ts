import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ComparisonPairModel } from '../models/comparison-pair.model';
import { OptionModel } from '../models/option.model';

@Injectable({ providedIn: 'root' })
export class ComparisonService implements OnDestroy {

	public options$ = new BehaviorSubject<Array<OptionModel>>([]);
	public pairs$ = new BehaviorSubject<Array<ComparisonPairModel>>([]);
	public isCompleted$ = new BehaviorSubject<boolean>(false);

	private id = 0;

	private readonly destroyed$ = new Subject<undefined>();

	constructor() {
		this.subscribeDataChanges();
	}

	ngOnDestroy(): void {
		this.destroyed$.next();
		this.destroyed$.complete();
	}


	public hasOption(value: string): boolean {
		const options = this.options$.value;
		const index = options.findIndex(item => item.value === value);

		return index > -1;
	}

	public addOption(value: string): void {
		const options = this.options$.value;
		const result = [
			...options,
			{
				id: (this.id++).toString(),
				value,
				rate: 0
			}
		];

		this.options$.next(result);

		const pairs = this.updatePairs(result, this.pairs$.value);
		this.pairs$.next(pairs);
	}

	public removeOption(id: string): void {
		const options = this.options$.value;
		const index = options.findIndex(item => item.id === id);
		if (index > -1) {
			const result = [
				...options.slice(0, index),
				...options.slice(index + 1)
			];

			this.options$.next(result);

			const pairs = this.updatePairs(result, this.pairs$.value);
			this.pairs$.next(pairs);
		}
	}

	public updatePair(pair: ComparisonPairModel): void {
		const pairs = this.pairs$.value;
		const index = pairs.findIndex(item => item.id === pair.id);
		if (index > -1) {
			const result = [
				...pairs.slice(0, index),
				pair,
				...pairs.slice(index + 1)
			];

			this.pairs$.next(result);
		}
	}


	private subscribeDataChanges(): void {
		this.pairs$
			.pipe(takeUntil(this.destroyed$))
			.subscribe(data => {
				const options = this.updateOptions(data, this.options$.value);
				this.options$.next(options);

				const isCompleted = this.checkIfIsCompleted(data);
				this.isCompleted$.next(isCompleted);
			});
	}


	private updateOptions = (pairs: Array<ComparisonPairModel>, options: Array<OptionModel>): Array<OptionModel> => {
		const result: Array<OptionModel> = [];

		options.forEach(option => {
			const rate = pairs.filter(pair => {
				return (pair.selected === 0 && pair.options[0].id === option.id)
					|| (pair.selected === 1 && pair.options[1].id === option.id);
			}).length;

			result.push({ ...option, rate });
		});

		return result;
	};

	private updatePairs = (options: Array<OptionModel>, pairs: Array<ComparisonPairModel>): Array<ComparisonPairModel> => {
		const result: Array<ComparisonPairModel> = [];

		options.forEach((value1, index1) => {
			options.forEach((value2, index2) => {
				if (index1 < index2) {
					const id = value1.id + value2.id;

					let pair = pairs.find(item => item.id === id);
					if (!pair) {
						pair = {
							id,
							options: [value1, value2],
							selected: -1
						};
					}

					result.push(pair);
				}
			});
		});

		return result;
	};

	private checkIfIsCompleted = (data: Array<ComparisonPairModel>): boolean => {
		const notSelected = data.filter(pair => {
			return pair.selected === -1;
		});

		return data.length > 0 && notSelected.length === 0;
	};

}
