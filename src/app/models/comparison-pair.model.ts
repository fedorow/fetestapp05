import { OptionModel } from './option.model';

export class ComparisonPairModel {

	id: string;
	options: [OptionModel, OptionModel];
	selected: number;

}
